package switch2019.project.infrastructureLayer.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.AddressJpa;

import java.util.List;
import java.util.Optional;

public interface AddressJpaRepository extends CrudRepository<AddressJpa, Long> {

    Optional<AddressJpa> findById(long id);
    List<AddressJpa> findAll();
}
