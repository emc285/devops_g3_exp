package switch2019.project.infrastructureLayer.dataPersistence.repositoriesJPA;

import org.springframework.data.repository.CrudRepository;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.LedgerJpa;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.TransactionJpa;

import java.util.List;
import java.util.Optional;


public interface TransactionJpaRepository extends CrudRepository<TransactionJpa, Long> {

    List<TransactionJpa> findAll();

    Optional<TransactionJpa> findById(Long id);

    List<TransactionJpa> findAllByLedger(LedgerJpa id);
}