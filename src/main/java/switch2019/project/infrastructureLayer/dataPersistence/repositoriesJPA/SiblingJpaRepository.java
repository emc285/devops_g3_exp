package switch2019.project.infrastructureLayer.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.SiblingJpa;

import java.util.List;

public interface SiblingJpaRepository extends CrudRepository<SiblingJpa, Long> {

    SiblingJpa findById(long id);
    //List<AdminJpa> findAllByGroupId( GroupId id);
    List<SiblingJpa> findAll();
}
