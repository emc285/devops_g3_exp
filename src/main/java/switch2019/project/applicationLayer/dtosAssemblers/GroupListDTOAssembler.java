package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtos.GroupIDDTO;
import switch2019.project.applicationLayer.dtos.GroupListDTO;

import java.util.List;

public class GroupListDTOAssembler {

    public static GroupListDTO createDTOFromDomainObject(List<GroupDTO> listOfGroupDTO) {

        GroupListDTO groupListDTO = new GroupListDTO(listOfGroupDTO);
        return  groupListDTO;
    }
}
