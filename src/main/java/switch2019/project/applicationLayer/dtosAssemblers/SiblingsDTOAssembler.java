package switch2019.project.applicationLayer.dtosAssemblers;

import switch2019.project.applicationLayer.dtos.SiblingsDTO;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;

import java.util.ArrayList;
import java.util.List;

public class SiblingsDTOAssembler {

    public static SiblingsDTO createDTOFromDomainObject(List<PersonID> siblings) {
        List<String> personSiblings = new ArrayList<>();

        for(PersonID personID : siblings){
            personSiblings.add(personID.getEmail().getEmail());
        }

        SiblingsDTO siblingsDTO = new SiblingsDTO(personSiblings);
        return siblingsDTO;
    }
}
