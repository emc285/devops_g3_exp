package switch2019.project.infrastructureLayer.dataPersistence.dataAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.domainLayer.domainEntities.aggregates.category.Category;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.entitiesInterfaces.OwnerID;
import switch2019.project.infrastructureLayer.dataPersistence.dataModel.CategoryJpa;

import static org.junit.jupiter.api.Assertions.*;

class CategoryDomainDataAssemblerTest {

    @Test
    @DisplayName("CategoryDomainDataAssembler - Test create CategoryJpa to data")

    public void categoryDomainDataAssembler_toData () {

        //Arrange

        String id = "alexandre@gmail.com";
        OwnerID ownerID = PersonID.createPersonID("alexandre@gmail.com");
        String categoryDenomination = "Salary";

        Category category = Category.createCategory(categoryDenomination, ownerID);

        //Act

        CategoryDomainDataAssembler categoryDomainDataAssembler = new CategoryDomainDataAssembler();
        CategoryJpa categoryJpa = categoryDomainDataAssembler.toData(category);

        //Assert

        assertEquals(id, categoryJpa.getId().getOwnerID());
        assertEquals(categoryDenomination, categoryJpa.getId().getDenomination());
    }

    @Test
    @DisplayName("CategoryDomainDataAssembler - Test create CategoryJpa to domain")

    public void categoryDomainDataAssembler_toDomain () {

        //Arrange

        String id = "alexandre@gmail.com";
        OwnerID ownerID = PersonID.createPersonID("alexandre@gmail.com");
        String categoryDenomination = "Salary";

        Category category = Category.createCategory(categoryDenomination, ownerID);

        //Act

        CategoryDomainDataAssembler categoryDomainDataAssembler = new CategoryDomainDataAssembler();
        CategoryJpa categoryJpa = categoryDomainDataAssembler.toData(category);
        Category newCategory = categoryDomainDataAssembler.toDomain(categoryJpa);

        //Assert

        assertEquals(ownerID, newCategory.getCategoryID().getOwnerID());
        assertEquals(categoryDenomination, newCategory.getCategoryID().getDenomination().getDenomination());
    }

}