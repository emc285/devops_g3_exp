package switch2019.project.applicationLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.applicationLayer.dtos.AccountDTO;
import switch2019.project.applicationLayer.dtos.AccountsDTO;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.entitiesInterfaces.OwnerID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountsDTOAssemblerTest {

    @Test
    @DisplayName("AccountsDTOAssembler - Test create AccountsDTO from domain objects")
    void accountsDTOAssembler_createDTOFromDomainObject() {

        //Arrange

        String accountDenomination = "Supermarket Account";
        String accountDescription = "Supermarket expenses";

        AccountDTO accountDTO = AccountDTOAssembler.createDTOFromPrimitiveTypes(accountDenomination, accountDescription);

        List<AccountDTO> listAccountDTO = new ArrayList<>();
        listAccountDTO.add(accountDTO);

        //Act
        AccountsDTOAssembler accountsDTOAssembler = new AccountsDTOAssembler();
        AccountsDTO accountsDTO = accountsDTOAssembler.createDTOFromDomainObject(listAccountDTO);

        //Expected
        AccountsDTO accountsDTOExpected = new AccountsDTO(listAccountDTO);

        //Assert
        assertEquals(accountsDTOExpected, accountsDTO);
    }
}