package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.applicationServices.US005_1CreateGroupCategoryService;
import switch2019.project.applicationLayer.dtos.CreateGroupCategoryDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtosAssemblers.CreateGroupCategoryDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US005_1CreateGroupCategoryController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.vosShared.DateOfCreation;
import switch2019.project.domainLayer.domainEntities.vosShared.Denomination;
import switch2019.project.domainLayer.domainEntities.vosShared.Description;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class US005_1CreateGroupCategoryControllerTest extends AbstractTest {

    @Mock
    private US005_1CreateGroupCategoryService service;


    //SUCCESS

    @Test
    @DisplayName("Test For createCategoryAsPeopleInCharge() | Success")
    public void whenGroupCategoryIsCreated_thenRetrievedMsgIsSuccess() {
        //Arrange

        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";
        String categoryDenomination = "Allowance";

        //Expected result
        Denomination denomination = Denomination.createDenomination(groupDenomination);
        Description description = Description.createDescription(groupDescription);
        DateOfCreation dateOfCreation = DateOfCreation.createDateOfCreation(LocalDate.now());
        GroupDTO isCategoryCreatedExpected = GroupDTOAssembler.createDTOFromDomainObject(denomination, description, dateOfCreation);

        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, categoryDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createCategoryAsPeopleInCharge(createGroupCategoryDTO)).thenReturn(isCategoryCreatedExpected);

        //Controller
        US005_1CreateGroupCategoryController controller = new US005_1CreateGroupCategoryController(service);

        //Act

        GroupDTO result = controller.createCategoryAsPeopleInCharge(personEmail, groupDenomination, categoryDenomination);

        //Assert
        assertEquals(isCategoryCreatedExpected, result);
    }

    //PERSON_NOT_IN_CHARGE

    @Test
    @DisplayName("Test For createCategoryAsPeopleInCharge() | People Not In Charge")
    public void whenGroupCategoryIsCreated_thenRetrievedMsgIsPersonNotInCharge() {
        //Arrange

        String personEmail = "paulo@gmail.com";
        String groupDenomination = "Fontes Family";
        String categoryDenomination = "Allowance";

        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, categoryDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createCategoryAsPeopleInCharge(createGroupCategoryDTO)).thenThrow(new InvalidArgumentsBusinessException(US005_1CreateGroupCategoryService.PERSON_NOT_IN_CHARGE));

        //Controller
        US005_1CreateGroupCategoryController controller = new US005_1CreateGroupCategoryController(service);

        //Act

        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controller.createCategoryAsPeopleInCharge(personEmail, groupDenomination, categoryDenomination));

        //Assert
        assertEquals(thrown.getMessage(), US005_1CreateGroupCategoryService.PERSON_NOT_IN_CHARGE);
    }

    //CATEGORY_ALREADY_EXIST

    @Test
    @DisplayName("Test For createCategoryAsPeopleInCharge() | Category Already Exists")
    public void whenGroupCategoryIsCreated_thenRetrievedMsgIsCategoryAlreadyExists() {
        //Arrange

        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Fontes Family";
        String categoryDenomination = "Salary";

        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, categoryDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createCategoryAsPeopleInCharge(createGroupCategoryDTO)).thenThrow(new InvalidArgumentsBusinessException(US005_1CreateGroupCategoryService.CATEGORY_ALREADY_EXIST));

        //Controller
        US005_1CreateGroupCategoryController controller = new US005_1CreateGroupCategoryController(service);

        //Act

        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controller.createCategoryAsPeopleInCharge(personEmail, groupDenomination, categoryDenomination));

        //Assert
        assertEquals(thrown.getMessage(), US005_1CreateGroupCategoryService.CATEGORY_ALREADY_EXIST);
    }

    //GROUP_DOES_NOT_EXIST

    @Test
    @DisplayName("Test For createCategoryAsPeopleInCharge() | Group Does Not Exist")
    public void whenGroupCategoryIsCreated_thenRetrievedMsgIsGroupDoesNotExists() {
        //Arrange

        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Santos Family";
        String categoryDenomination = "Allowance";

        CreateGroupCategoryDTO createGroupCategoryDTO = CreateGroupCategoryDTOAssembler.createDTOFromPrimitiveTypes(personEmail, groupDenomination, categoryDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createCategoryAsPeopleInCharge(createGroupCategoryDTO)).thenThrow(new NotFoundArgumentsBusinessException(US005_1CreateGroupCategoryService.GROUP_DOES_NOT_EXIST));

        //Controller
        US005_1CreateGroupCategoryController controller = new US005_1CreateGroupCategoryController(service);

        //Act

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> controller.createCategoryAsPeopleInCharge(personEmail, groupDenomination, categoryDenomination));

        //Assert
        assertEquals(thrown.getMessage(), US005_1CreateGroupCategoryService.GROUP_DOES_NOT_EXIST);
    }
}