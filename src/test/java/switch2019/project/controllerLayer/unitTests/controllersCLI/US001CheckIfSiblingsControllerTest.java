package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.applicationServices.US001CheckIfSiblingsService;
import switch2019.project.applicationLayer.dtos.BooleanDTO;
import switch2019.project.applicationLayer.dtos.CheckIfSiblingsDTO;
import switch2019.project.applicationLayer.dtosAssemblers.BooleanDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.CheckIfSiblingsDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US001CheckIfSiblingsController;
import switch2019.project.controllerLayer.controllers.controllersREST.US001_CheckIfSiblingsControllerRest;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Ala Matos
 */

class US001CheckIfSiblingsControllerTest extends AbstractTest {

        @Mock
        private US001CheckIfSiblingsService us001CheckIfSiblingsService;

        @Test
        @DisplayName("Test Controller_US01 - hulk | wolverine | siblings")
        void controller_US01() {

            //Arrange people
            String hulkEmail = "hulk@marvel.com";
            String wolverineEmail = "wolverine@marvel.com";


            //Arrange expected result

            boolean result = true;
            String expectedMsg = US001CheckIfSiblingsService.SUCCESS;
            BooleanDTO expectedSiblingsAnalysis = BooleanDTOAssembler.createDTOFromPrimitiveTypes(result, expectedMsg);


            //Arrange checkIfSiblingsDTO

            CheckIfSiblingsDTO checkIfSiblingsDTO = CheckIfSiblingsDTOAssembler.createDTOFromPrimitiveTypes(hulkEmail, wolverineEmail);


            //Act
            // Mock the behaviour of the service's checkIfSiblings method,
            // so it does not depend on other parts (e.g. DB)
            Mockito.when(us001CheckIfSiblingsService.checkIfSiblings(checkIfSiblingsDTO))
                    .thenReturn(expectedSiblingsAnalysis);

            //Act actual object

            US001CheckIfSiblingsController us001CheckIfSiblingsController = new US001CheckIfSiblingsController(us001CheckIfSiblingsService);
            BooleanDTO actualResultSiblings= us001CheckIfSiblingsController.checkIfBrother(hulkEmail, wolverineEmail);

            //Assert

            assertEquals(expectedSiblingsAnalysis, actualResultSiblings);
        }

        @Test
        @DisplayName("Test Controller_US01 - hulk | ironMan | Not siblings")
        void controller_US01NotSiblings() {

            //Arrange people

            String hulkEmail = "hulk@marvel.com";
            String iroManEmail = "ironman@marvel.com";


            //Arrange expected result

            boolean resultNotSiblings = false;
            String expectedMsgNotSiblings = US001CheckIfSiblingsService.FAIL;
            BooleanDTO expectedNotSiblingsAnalysis = BooleanDTOAssembler.createDTOFromPrimitiveTypes(resultNotSiblings, expectedMsgNotSiblings);

            //Arrange checkIfSiblingsDTO

            CheckIfSiblingsDTO checkINotSiblingsDTO = CheckIfSiblingsDTOAssembler.createDTOFromPrimitiveTypes(hulkEmail, iroManEmail);

            //Act
            // Mock the behaviour of the service's checkIfSiblings method,
            // so it does not depend on other parts (e.g. DB)
            Mockito.when(us001CheckIfSiblingsService.checkIfSiblings(checkINotSiblingsDTO))
                    .thenReturn(expectedNotSiblingsAnalysis);

            //Act actual object

            US001CheckIfSiblingsController us001CheckIfSiblingsController = new US001CheckIfSiblingsController(us001CheckIfSiblingsService);
            BooleanDTO actualResultNotSiblings= us001CheckIfSiblingsController.checkIfBrother(hulkEmail, iroManEmail);

            //Assert
            assertEquals(expectedNotSiblingsAnalysis, actualResultNotSiblings);
        }

        @Test
        @DisplayName("Test Controller_US01 - Empty Object")
        void controller_US01EmptyObject() throws RuntimeException {

            //Arrange checkIfSiblingsDTO
            CheckIfSiblingsDTO checkINotSiblingsDTO = CheckIfSiblingsDTOAssembler.createDTOFromPrimitiveTypes(null, null);

            //Act
            // Mock the behaviour of the service's checkIfSiblings method,
            // so it does not depend on other parts (e.g. DB)
            Mockito.when(us001CheckIfSiblingsService.checkIfSiblings(checkINotSiblingsDTO))
                    .thenThrow(new RuntimeException("Requested body empty. Please add some info accordingly."));

            //Act actual object
            US001CheckIfSiblingsController us001CheckIfSiblingsController = new US001CheckIfSiblingsController(us001CheckIfSiblingsService);

            //Act expected object
            Throwable thrown = assertThrows(RuntimeException.class, () -> us001CheckIfSiblingsController.checkIfBrother(null, null));
            String message = thrown.getMessage();
            assertEquals(thrown.getMessage(), "Requested body empty. Please add some info accordingly.");

        }
}
