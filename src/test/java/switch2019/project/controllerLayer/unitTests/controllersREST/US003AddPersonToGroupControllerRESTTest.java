package switch2019.project.controllerLayer.unitTests.controllersREST;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import switch2019.project.applicationLayer.applicationServices.US003AddPersonToGroupService;
import switch2019.project.applicationLayer.dtos.AddPersonToGroupDTO;
import switch2019.project.applicationLayer.dtos.GroupDTO;
import switch2019.project.applicationLayer.dtos.NewAddPersonToGroupInfoDTO;
import switch2019.project.applicationLayer.dtosAssemblers.AddPersonToGroupDTOAssembler;
import switch2019.project.applicationLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersREST.US002_1CreateGroupControllerREST;
import switch2019.project.controllerLayer.controllers.controllersREST.US003AddPersonToGroupControllerREST;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.vosShared.DateOfCreation;
import switch2019.project.domainLayer.domainEntities.vosShared.Denomination;
import switch2019.project.domainLayer.domainEntities.vosShared.Description;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


public class US003AddPersonToGroupControllerRESTTest extends AbstractTest {

    @Mock
    private US003AddPersonToGroupService serviceUS003;

    @Autowired
    private US003AddPersonToGroupControllerREST controllerRESTUS003;


/*    @Test
    @DisplayName("PostMapping - Success")
    public void whenPersonIsAddedTogroupMsgIsSucess() {

        //Arrange

        String email = "alexandre@gmail.com";
        String gDenomination = "Sunday Runners";
        String gDescription = "All members from Sunday Runners group";

        //Arrange expected result

        Denomination denomination = Denomination.createDenomination(gDenomination);
        Description description = Description.createDescription(gDescription);
        DateOfCreation dateOfCreation = DateOfCreation.createDateOfCreation(LocalDate.now());
        GroupDTO personIsAddedToGroup= GroupDTOAssembler.createDTOFromDomainObject(denomination, description, dateOfCreation);

        // Arrange info to DTO

        NewAddPersonToGroupInfoDTO newAddPersonToGroupInfoDTO = new NewAddPersonToGroupInfoDTO(email);

        // Arrange addPersonToGroupDTO

        AddPersonToGroupDTO addPersonToGroupDTO = AddPersonToGroupDTOAssembler.createDataTransferObject_Primitives(newAddPersonToGroupInfoDTO.getEmail(), gDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)

        Mockito.when(serviceUS003.addPersonToGroup(addPersonToGroupDTO)).thenReturn(personIsAddedToGroup);

        //Act Expected object

        ResponseEntity<Object> expectedResponse = new ResponseEntity<>(personIsAddedToGroup, HttpStatus.CREATED);

        // Act actual result

        ResponseEntity<Object> isPersonAddedToGroup = controllerRESTUS003.addPersonToGroupP(newAddPersonToGroupInfoDTO, gDenomination);

        //Assert
        assertEquals(expectedResponse, isPersonAddedToGroup);
    }*/


    @Test
    @DisplayName("PostMapping : Person does not exist")
    public void whenPersonIsNotAddedTogroupMsgISPERSON_DOES_NOT_EXIST() {

        //Arrange

        String email = "crispim@gmail.com";
        String gDenomination = "Sunday Runners";

        NewAddPersonToGroupInfoDTO newAddPersonToGroupInfoDTO = new NewAddPersonToGroupInfoDTO(email);

        AddPersonToGroupDTO addPersonToGroupDTO = AddPersonToGroupDTOAssembler.createDataTransferObject_Primitives(newAddPersonToGroupInfoDTO.getEmail(), gDenomination);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(serviceUS003.addPersonToGroup(addPersonToGroupDTO)).thenThrow(new InvalidArgumentsBusinessException(US003AddPersonToGroupService.PERSON_DOES_NOT_EXIST));

        //Act

        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controllerRESTUS003.addPersonToGroupP(newAddPersonToGroupInfoDTO, gDenomination));

        //Assert
        assertEquals(thrown.getMessage(), US003AddPersonToGroupService.PERSON_DOES_NOT_EXIST);
    }


//    @Test
//    @DisplayName("PostMapping : Person is already a member")
//    public void whenPersonIsNotAddedTogroupMsgISPERSON_ALREADY_EXIST_IN_THE_GROUP() {
//
//        //Arrange
//
//        String email = "paulo@gmail.com";
//        String gDenomination = "Sunday Runners";
//
//        NewAddPersonToGroupInfoDTO newAddPersonToGroupInfoDTO = new NewAddPersonToGroupInfoDTO(email);
//
//        AddPersonToGroupDTO addPersonToGroupDTO = AddPersonToGroupDTOAssembler.createDataTransferObject_Primitives(newAddPersonToGroupInfoDTO.getEmail(), gDenomination);
//
//        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
//        // so it does not depend on other parts (e.g. DB)
//        Mockito.when(serviceUS003.addPersonToGroup(addPersonToGroupDTO)).thenThrow(new InvalidArgumentsBusinessException(US003AddPersonToGroupService.PERSON_ALREADY_EXIST_IN_THE_GROUP));
//
//        //Act
//
//        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> controllerRESTUS003.addPersonToGroupP(newAddPersonToGroupInfoDTO, gDenomination));
//
//        //Assert
//        assertEquals(thrown.getMessage(), US003AddPersonToGroupService.PERSON_ALREADY_EXIST_IN_THE_GROUP);
//
//    }


}
